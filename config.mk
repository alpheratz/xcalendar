# Copyright (C) 2022-2023 <alpheratz99@protonmail.com>
# This program is free software.

VERSION=0.3.16
CC=cc
INCS=-I/usr/include/freetype2 -I/usr/X11R6/include
CFLAGS=-std=c99 -pedantic -Wall -Wextra -Os $(INCS) -DVERSION=\"$(VERSION)\"
LDLIBS=-lxcb -lfreetype -lxcb-image -lxcb-keysyms -lfontconfig
LDFLAGS=-L/usr/X11R6/lib -s
PREFIX=/usr/local
MANPREFIX=$(PREFIX)/share/man
APPPREFIX=$(PREFIX)/share/applications
